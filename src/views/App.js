
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {createStackNavigator} from 'react-navigation';
import Tabbar from './Tabbar';
import Music from './Music';


const instructions = Platform.select({
  ios: '',
  android:''
});

const App = createStackNavigator({
  Main: {
    screen: Tabbar,
    navigationOptions: ({navigation}) => ({
      // header: null
    })
  },//tabbar
  Music: {screen: Music}
});

export default App;
