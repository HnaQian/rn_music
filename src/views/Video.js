

import React, {Component} from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';

const {width, height} = Dimensions.get('window');

export default class Video extends Component {
  static navigationOptions = {
    //stackNavigator的属性
    headerTitle: '视频',
    gestureResponseDistance: {horizontal: 300},
    headerStyle: {backgroundColor: '#C82E2B'},//导航栏的样式
    headerTitleStyle: {//导航栏文字的样式
        color: '#fff',
        //设置标题的大小
        fontSize: 16,
        //居中显示
        alignSelf: 'center'
    },
  }
  render() {
    return (
      <View style={styles.Video}>
      
      </View>
    )
  }
}

const styles = StyleSheet.create({
  Video: {
    backgroundColor: '#f1f1f1'
  }
})