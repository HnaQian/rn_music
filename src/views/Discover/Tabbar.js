import React, { Component } from 'react';
import {View, StyleSheet, Text, Animated, Easing} from 'react-native';
import {width, statusBarHeight, unitWidth} from './../../utils/Theme';

export default class Tabbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
        left1: new Animated.Value(150*unitWidth)
    }
  }
  componentDidMount() {
    
  }
  changeLineAnimation(current) {
    Animated.timing(this.state.left1, {
      toValue: (150+374*current)*unitWidth,       //属性目标值
      duration: 300,   
      easing: Easing.linear    
    }).start();         //执行动画
  }

  render() {
    const { tabbarCurrent, changeTabbarCurrent } = this.props
    return (
      <View style={styles.Tabbar}>
        <Text 
          style={ tabbarCurrent === 0 ? styles.tabbarCellAc : styles.tabbarCell }
          onPress={() => {changeTabbarCurrent(0);this.changeLineAnimation(0)}}
          >个性推荐</Text>
        <Text 
          style={ tabbarCurrent === 1 ? styles.tabbarCellAc : styles.tabbarCell }
          onPress={() => {changeTabbarCurrent(1);this.changeLineAnimation(1)}}
          >主播电台</Text>
        <Animated.View style={[styles.lineAnimation, {left: this.state.left1}]}></Animated.View>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  Tabbar: {
    backgroundColor: '#C82E2B',
    width: width,
    height: unitWidth*90,
    flexDirection: 'row',
    position: 'relative'
  },
  tabbarCell: {
    width: width/2,
    height: unitWidth*90,
    color: '#ffffff',
    lineHeight: unitWidth*70,
    textAlign: 'center',
    fontSize: unitWidth*28
  },
  tabbarCellAc: {
    width: width/2,
    height: unitWidth*90,
    color: '#ffffff',
    lineHeight: unitWidth*70,
    textAlign: 'center',
    fontSize: unitWidth*29,
    fontWeight: 'bold'
  },
  lineAnimation: {
    width: unitWidth*80,
    height: unitWidth*6,
    backgroundColor: '#ffffff',
    borderRadius: unitWidth*3,
    position: 'absolute',
    top: unitWidth*66,
  }
})
