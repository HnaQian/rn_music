import React, {Component} from 'react';
import {
  View, 
  StyleSheet,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';
import {width, unitWidth} from './../../utils/Theme';

const yuyin = require('./../../images/yuyin.png');
const paihang = require('./../../images/paihang.png');
const search = require('./../../images/search.png');

export default class Music extends Component {

  render() {
    return (
      <View style={styles.search}>
        <Image style={styles.yuyin} source={yuyin}></Image>
        <TouchableOpacity onPress={()=> {this.props.navigate('Music')}}>
          <View style={styles.searchInput}>
            <Image style={styles.searchicon} source={search}></Image>
            <Text style={styles.searchtext}>{this.props.searchHot}</Text>
          </View>
        </TouchableOpacity>
        
        <Image style={styles.paihang} source={paihang}></Image>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  search: {
    backgroundColor: '#C82E2B',
    width: width,
    height: unitWidth*90,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  yuyin: {
    width: unitWidth*55,
    height: unitWidth*55
  },
  searchInput: {
    width: unitWidth*530,
    height: unitWidth*60,
    borderRadius: unitWidth*30,
    backgroundColor: '#D2504C',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchicon: {
    width: unitWidth*30,
    height: unitWidth*30
  },
  searchtext: {
    fontSize: unitWidth*26,
    color: '#ffffff',
    marginLeft: unitWidth*10
  },
  paihang: {
    width: unitWidth*60,
    height: unitWidth*60
  }
})