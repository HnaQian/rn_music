



import React, {Component} from 'react';
import {View, StyleSheet, Image, Text,} from 'react-native';
import {width, statusBarHeight, unitWidth} from './../../utils/Theme';


export default class Discover extends Component {
  constructor(props) {
    super(props)

  }

  render() {
    
    return (
      <View style={styles.FourBtn}>
      {
        this.props.fourBtn.map((v, k) => {
          return (
            <View style={styles.fBtnCell} key={k}>
              <View style={styles.fBtnCellImgView}>
                <Image style={styles.fBtnCellImg} source={v.img}></Image>
              </View>
              <Text style={styles.fBtnCellTitle}>{v.name}</Text>
            </View>
          )
        })
      }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  FourBtn: {
    width: width,
    height: 210*unitWidth,
    backgroundColor: '#fff',
    marginTop: unitWidth*116,
    borderBottomColor: '#f1f1f1',
    borderBottomWidth: 2*unitWidth,
    borderStyle: 'solid',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  fBtnCell: {
    width: width/4,
    height: 210*unitWidth,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  fBtnCellImgView: {
    width: 100*unitWidth,
    height: 100*unitWidth,
    backgroundColor: '#C82E2B',
    borderRadius: 50*unitWidth,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  fBtnCellImg: {
    width: 60*unitWidth,
    height: 60*unitWidth
  },
  fBtnCellTitle: {
    fontSize: 23*unitWidth,
    color: '#333',
    marginTop: 16*unitWidth
  }
})