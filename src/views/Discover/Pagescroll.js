import React, {Component} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {width, height, statusBarHeight, unitWidth} from './../../utils/Theme';
import Recommend from './Recommend';
import Station from './Station';
export default class Pagescroll extends Component {

  constructor(props) {
    super(props);
    this.onMomentumScrollEnd = this.onMomentumScrollEnd.bind(this);
  }
  onMomentumScrollEnd(e) {
    const offSetX = e.nativeEvent.contentOffset.x;
    const currentIndex = offSetX/width;
    this.props.changeTabbarCurrent(currentIndex)
  }
  changeScrollViewCurrent(current) {
    var ScrollView = this.refs.ScrollView;
    ScrollView.scrollTo({x: width*current, y: 0, animated: true});
  }
  
  render() {
    
    return (
      <ScrollView
        ref="ScrollView"
        style={styles.Pagescroll}
        horizontal={true}
        contentContainerStyle={styles.scrollviewC}
        pagingEnabled={true}
        bounces={false}
        onMomentumScrollEnd={this.onMomentumScrollEnd}
        >
        <Recommend></Recommend>
        <Station></Station>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  Pagescroll: {
    width: width,
    height: height - unitWidth*(statusBarHeight + 180),
  },
  scrollviewC: {
    width: width*2,
    height: height - unitWidth*(statusBarHeight + 180),
    backgroundColor: '#fff'
  },
  recommend: {
    width: width,
    height: height - unitWidth*(statusBarHeight + 180),
    backgroundColor: '#ffffff'
  }
})