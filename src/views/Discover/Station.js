import React, {PureComponent} from 'react';
import {View, StyleSheet, FlatList, } from 'react-native';
import {width, height, statusBarHeight, unitWidth} from './../../utils/Theme';

export default class Station extends PureComponent {

  constructor(props) {
    super(props);
    this.onMomentumScrollEnd = this.onMomentumScrollEnd.bind(this);
  }
  onMomentumScrollEnd(e) {
  }
  
  render() {
    
    return (
      <FlatList
        style={styles.Station}
        pagingEnabled={true}
        bouncesZoom={true}
        onMomentumScrollEnd={this.onMomentumScrollEnd}
        >

      </FlatList>
    )
  }
}

const styles = StyleSheet.create({
  Station: {
    width: width,
    height: height - unitWidth*(statusBarHeight + 180),
    backgroundColor: '#fff'
  }
})