

import React, {Component} from 'react';
import {View, Dimensions, StyleSheet, Image, Text, StatusBar} from 'react-native';
import {width, statusBarHeight, unitWidth} from './../../utils/Theme';
import Search from './Search';
import Tabbar from './Tabbar';
import api from './../../api/discover';
import Pagescroll from './Pagescroll'

export default class Discover extends Component {
  constructor(props) {
    super(props)
    this.changeTabbarCurrent = this.changeTabbarCurrent.bind(this);
    this.state = {
      searchHot: '',
      tabbarCurrent: 0,
    }
  }
  static navigationOptions = {
    header: null
  }
  changeTabbarCurrent(current) {
    this.setState({
      tabbarCurrent: current
    })
    this.refs.Pagescroll.changeScrollViewCurrent(current);
    this.refs.Tabbar.changeLineAnimation(current);
  }
  componentDidMount() {
    api.searchSuggest().then((res) => {
      if (res.code === 200) {
        this.setState({
          searchHot: res.result.hots[0].first
        })
      }
    }).catch((err) => {
      
    });
  }
  render() {
    
    return (
      <View style={styles.discover}>
        <StatusBar barStyle={'light-content'} />
        <View style={styles.statusBar}></View>
        <Search searchHot={this.state.searchHot} navigate={this.props.navigation.navigate}></Search>
        <Tabbar ref='Tabbar' tabbarCurrent={this.state.tabbarCurrent} changeTabbarCurrent={this.changeTabbarCurrent}></Tabbar>
        <Pagescroll ref='Pagescroll' changeTabbarCurrent={this.changeTabbarCurrent}></Pagescroll>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  discover: {
    backgroundColor: '#f1f1f1'
  },
  statusBar: {
    width: width,
    height: statusBarHeight,
    backgroundColor: '#C82E2B'
  }
})