
/**
 * 轮播图
 * 实现原理：n+2个slider的方式，暂时3个slider的方式不想写，后面再补充
 * 详细描述：
 *    
 */

import React, {Component} from 'react';
import {
  View, 
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import {width, unitWidth} from './../../utils/Theme';

export default class Banner extends Component {
  constructor(props) {
    super(props);
    this.bannerAnimationEnd = this.bannerAnimationEnd.bind(this)
    this.state = {
      currentDot: 0,//banner小红点，默认是第一个
      current: 1
    }
  }

  componentDidMount() {
    if (this.props.banners.length > 0) {
      const bannersLength = this.props.banners.length;
      var ScrollView = this.refs.Banner;

      setInterval(() => {
        ScrollView.scrollTo({x: width*(this.state.current+1), y: 0, animated: true});
      }, 4000)
    }
  }

  //当滚动停止时，判断位置，实现循环
  bannerAnimationEnd(e) {
    
    const bannersLength = this.props.banners.length;
    const offSetX = e.nativeEvent.contentOffset.x;
    const currentIndex = offSetX/width;
    var ScrollView = this.refs.Banner;
    if (currentIndex === 0) {
      ScrollView.scrollTo({x: width*(bannersLength-2), y: 0, animated: false});
    } else if(currentIndex === bannersLength-1) {
      ScrollView.scrollTo({x: width, y: 0, animated: false});
    }
    var dotIndex = currentIndex - 1
    if (dotIndex < 0) {
      dotIndex = this.props.banners.length - 3
    }else if (dotIndex == this.props.banners.length - 2){
      dotIndex = 0
    }
    this.setState({
      currentDot: dotIndex,
      current: currentIndex == bannersLength - 2 ? 0 : currentIndex
    })
  } 

  render() {
    
    
    return (
      <View style={styles.Banner}>
        <ScrollView 
          ref='Banner'
          style={styles.Banner}
          contentContainerStyle={{
            width: width*this.props.banners,
            height: unitWidth*356,
          }}
          pagingEnabled={true}//按页滚动
          horizontal={true}//横向
          bounces={false}//ios弹簧
          showsHorizontalScrollIndicator={false}
          contentOffset={{x: width, y: 0}}//初始化偏移量
          onMomentumScrollEnd={this.bannerAnimationEnd}
          >
          {
            this.props.banners.map((v, k) => {
              return (
                <Image style={styles.bannerImage} source={{uri: v.picUrl}} key={k}></Image>
              )
            })
          }
        </ScrollView>
        <View style={styles.bannerDot}>
        {
          this.props.banners.map((v, k) => {
            if (k < this.props.banners.length -2) {
              return (
                <View 
                  style={[styles.dotCell, {backgroundColor: this.state.currentDot == k ? '#C82E2B' : '#f1f1f1'}]} 
                  key={'dot-'+k}
                  ></View>
              )
            }
          })
        }
        </View>
      </View>
      
    )
  }
}

const styles = StyleSheet.create({
  Banner: {
    width: width,
    height: unitWidth*356,
    position: 'absolute',
    zIndex: 1
  },
  contentBanner: {
    height: unitWidth*356,
  },
  bannerImage: {
    width: unitWidth*730,
    height: unitWidth*336,
    margin: unitWidth*10,
    borderRadius: unitWidth*10
  },
  bannerDot: {
    height: unitWidth*40,
    width: width,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    zIndex: 4,
    bottom: unitWidth*20
  },
  dotCell: {
    width: unitWidth*14,
    height: unitWidth*14,
    borderRadius: unitWidth*7,
    marginLeft: unitWidth*6,
    marginRight: unitWidth*6,
    
  }
})