

import React, {Component} from 'react';
import {View, Dimensions, StyleSheet, Text, Image} from 'react-native';
import { unitWidth } from '../../../utils/Theme';

const {width, height} = Dimensions.get('window');

export default class SixBlock extends Component {

  componentDidMount() {
    const { cellData } = this.props
  }
  
  render() {
    const { cellData } = this.props
    return (
      <View style={styles.SixBlock}>
        <View style={styles.cellTitleView}>
          <Text style={styles.cellTitle}>{cellData.title}</Text>
          <View style={styles.cellTitleArrow}></View>
        </View>
        <View style={styles.sixList}>
        {
          cellData.listData.map((v, k) => {
            return (
              <View key={k+'-cell'} style={styles.cell}>
                <Image style={styles.cellImg} source={{uri: v.picUrl}} ></Image>
              
            
                
                <Text 
                  style={styles.cellImgTitle} 
                  numberOfLines={2}
                  >{v.name}</Text>
              </View>
            )
          })
        }
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  SixBlock: {
    backgroundColor: '#fff',
    width: width,
    paddingTop: unitWidth*30,
  },
  cellTitleView: {
    height: unitWidth*50,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: unitWidth*10
  },
  cellTitle: {
    fontSize: unitWidth*30,
    fontWeight: 'bold',
    color: '#000',
  },
  cellTitleArrow: {
    width: unitWidth*16,
    height: unitWidth*16,
    borderRightColor: '#333',
    borderRightWidth: unitWidth*2,
    borderTopColor: '#333',
    borderTopWidth: unitWidth*2,
    transform: [{rotate: '45deg'}],
    
  },
  sixList: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginLeft: unitWidth*12,
    marginRight: unitWidth*12,
    marginTop: unitWidth*20,
  },
  cell: {
    width: unitWidth*238,
    marginBottom: unitWidth*20,

    flexDirection: 'column'
  },
  cellImg: {
    width: unitWidth*238,
    height: unitWidth*238,
    borderRadius: unitWidth*10
  },
  cellImgTitle: {
    marginTop: unitWidth*10,
    marginLeft: unitWidth*6,
    width: unitWidth*226,
    fontSize: unitWidth*24,
    lineHeight: unitWidth*32
  }
})