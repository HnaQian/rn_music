import React, {PureComponent} from 'react';
import {View, StyleSheet, FlatList, Text} from 'react-native';
import {width, height, statusBarHeight, unitWidth} from './../../utils/Theme';
import api from './../../api/discover';
import Banner from './Banner';
import FourBtn from './FourBtn';
import SixBlock from './Cell/SixBlock';
export default class Recommend extends PureComponent {

  constructor(props) {
    super(props);
    this.onMomentumScrollEnd = this.onMomentumScrollEnd.bind(this);
    this.getPresonalizedData = this.getPresonalizedData.bind(this);
    this.getPresonalizedNewsongData = this.getPresonalizedNewsongData.bind(this);
    this.getPresonalizedDjprogramData = this.getPresonalizedDjprogramData.bind(this);
    this.state = {
      banners: [],
      fourBtn: [
        {
          name: '私人FM',
          img: require('./../../images/shouyinji.png'),
          path: ''
        },{
          name: '每日推荐',
          img: require('./../../images/rili.png'),
          path: ''
        },{
          name: '歌单',
          img: require('./../../images/gedanshouluon.png'),
          path: ''
        },{
          name: '排行榜',
          img: require('./../../images/paihang.png'),
          path: ''
        },
      ],
      flatList: []
    }
  }
  componentDidMount() {
    api.getBannerData().then((result) => {
      var banners = result.banners
      /**
       * 处理banner数据，将数据最后一个添加到开头，将第一个添加到最后
       */
      banners.unshift(banners[banners.length - 1]);
      banners.push(banners[1]);
      this.setState({ banners: result.banners });
    }).catch((err) => {
      alert(err, 1)
    });
    this.getPresonalizedData();
    
  }
  //推荐歌单
  getPresonalizedData() {
    api.getPresonalizedData().then((res) => {
      
      var presonalizedData = this.state.flatList
      presonalizedData.push({
        name: 'presonalize',
        listData: res.result.slice(0, 6),
        title: '推荐歌单'
      });
      this.setState({
        flatList: presonalizedData
      });
      this.getPresonalizedNewsongData();
    }).catch((err) => {
      console.warn(2)
      alert(err, 2);
    });
  }
  //最新音乐
  getPresonalizedNewsongData() {
    api.getPresonalizedNewsongData().then((res) => {
      res.result.forEach((v, k) => {
        res.result[k].picUrl = res.result[k].song.album.picUrl
      });
      var presonalizedData = this.state.flatList
      presonalizedData.push({
        name: 'personalizednewsong',
        listData: res.result.slice(0, 6),
        title: '最新音乐'
      });
      this.setState({
        flatList: presonalizedData
      });
      this.getPresonalizedDjprogramData();
    }).catch((err) => {
      console.warn(3)
      alert(err, 3);
    });
  }
  /**
   * 推荐电台
   */
  getPresonalizedDjprogramData() {
    api.getPresonalizedDjprogramData().then((res) => {
      var presonalizedData = this.state.flatList
      presonalizedData.push({
        name: 'personalizeddjprogram',
        listData: res.result.slice(0, 6),
        title: '推荐电台'
      });
      this.setState({
        flatList: presonalizedData
      });
    }).catch((err) => {
      console.warn(4)
    });
  }

  _renderItem = ({item}) => {
    return (
      <SixBlock cellData={item}></SixBlock>
    )
  }



  onMomentumScrollEnd(e) {
  }
  
  render() {

    const flatListHeader = () => {
      return (
        <View style={styles.flatListHeader}>
          {
            this.state.banners.length ? <Banner banners={this.state.banners}></Banner> : null
          }
          
          
          <View style={styles.flatListHeaderBg}></View>
          <FourBtn fourBtn={this.state.fourBtn}></FourBtn>
        </View>
        
      )
    }
    
    return (
      <FlatList
        style={styles.Recommend}
        pagingEnabled={false}
        bounces={true}
        onMomentumScrollEnd={this.onMomentumScrollEnd}
        ListHeaderComponent={flatListHeader}

        data={this.state.flatList}
        renderItem={this._renderItem}
        extraData={this.state}
        >

      </FlatList>
    )
  }
}

const styles = StyleSheet.create({
  Recommend: {
    width: width,
    height: height - unitWidth*(statusBarHeight + 180),
    backgroundColor: '#C82E2B'
  },
  flatListHeader: {
    width: width,
    height: unitWidth*556,
    backgroundColor: '#fff'
  },
  flatListHeaderBg: {
    width: width,
    height: unitWidth*230,
    backgroundColor: '#C82E2B'
  },
})