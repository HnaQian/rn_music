import api from './resources';

export default {
  /**
   * 搜索建议
   */
  searchSuggest: () => api.get('search/hot'),
  /**
   * banner
   */
  getBannerData: () => api.get('banner'),
  /**
   * 推荐歌单
   */
  getPresonalizedData: () => api.get('personalized'),
  /**
   * 最新音乐
   */
  getPresonalizedNewsongData: () => api.get('personalized/newsong'),
  /**
   * 推荐电台
   */
  getPresonalizedDjprogramData: () => api.get('personalized/djprogram'),
}
